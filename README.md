Lazy promises
=============

This library achieves lazy evaluation when using promises. This is necessary
when statements with side-effects are used with promises.

I want to achieve something that is equivalent to this `p1() && p2()` with
promises (here is `p2` only evaluated if `p1` is false).

Here is an example:

```
> P.deferred(console.log, this, 'hi')
{ then: [Function] }
> P.deferred(console.log, this, 'hi').then()
hi
```

`deferred` takes the arguments `funcArg`, `thisArg` and the arguments
that should be passed to `funcArg`. `deferred.then` will simply do
`funcArg.apply(thisArg, arguments)`

On the other hand, these two statements behave exactly the same way:

```
> Promise.resolve(console.log('hi'));
hi
{}
> Promise.resolve(console.log('hi')).then()
hi
{}
```

Lazy promises functions depend on the
[NodeJS promise library](https://www.npmjs.com/package/promise) but should
work with any Promise/A+ compatible framework.

The library also contain a number of functions for conditional evaluation of
promises. Here are some examples of how the library is used.

like `if(b()) { p() }`. `deferred` ensures that `p` is evaluated only if `b`
is true:
```
promiseIf(deferred(false), deferred(true) )
.then(function(x) {
  assert.equal(x, null, 'promiseIf should be null');
}).catch(console.log)
```

like `p1() && p2()`. `deferred` ensures that `p2` only is evaluated if `p1`
is true:
```
promiseAnd(deferred(true), deferred(true))
.then(function(x) {
  assert.equal(x, true, 'promiseAnd should be true');
}).catch(console.log)

```

like `p1() || p2()`. `deferred` ensures that `p2` only is evaluated if `p1`
is false:
```
promiseOr(deferred(false), deferred(true))
.then(function(x) {  
  assert.equal(x, true, 'promiseOr should be true');
}).catch(console.log)
```

like `!p()`:
```
promiseNot(deferred(null))
.then(function(x) {
  assert.equal(x, true, 'promiseNot should be true');
}).catch(console.log)
```


Contributions & License
-----------------------

All feedback is greatly appreciated! Just create a github issue or fork request.

Thank's to [Forbes Linsey](https://github.com/ForbesLindesay) for valuable
input.

License: MIT
