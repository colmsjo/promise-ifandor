var assert = require('assert');
var Promise = require('promise');
var P = require('./index.js');

console.log('No output means that the tests ran without errors!')

// throw an exception whenever the promise is resolved
var deferredThrow = function(e) {
  return P.deferred(function() {throw e});
};

// test deferred
var O = function() {
  this.val = 1;
};

O.prototype.add = function(num) {
  return this.val+num;
};

var o = new O();

P.deferred(o.add.bind(o, 1)).then(function(x){
  assert.equal(x, 2, 'deferred #1')
})

// test when
.then(function() {
  return P.when(P.deferred(function() {return true}),
         function(x) {return assert.equal(x, true, 'when #1');})
})

// test promiseNot
.then(function() {
  return P.promiseNot(Promise.resolve(true)).then(function(x) {
    return assert.equal(x, false, 'promiseNot #1');
  })
})
.then(function() {
  return P.promiseNot(true).then(function(x) {
    return assert.equal(x, false, 'promiseNot #2');
  })
})
.then(function() {
  return P.promiseNot(null).then(function(x) {
    return assert.equal(x, true, 'promiseNot #3');
  })
})

// test promiseIf
.then(function() {
  return P.promiseIf(false, Promise.resolve(true) ).then(function(x) {
    return assert.equal(x, false, 'promiseIf #1');
  })
})
.then(function() {
  return P.promiseIf(true, Promise.resolve(true) ).then(function(x) {
    return assert.equal(x, true, 'promiseIf #2');
  })
})

// test promiseAnd
.then(function() {
  return P.promiseAnd(true, Promise.resolve(true)).then(function(x) {
    return assert.equal(x, true, 'promiseAnd #1');
  })
})
.then(function() {
  return P.promiseAnd(false, Promise.resolve(true)).then(function(x) {
    return assert.equal(x, false, 'promiseAnd #2');
  })
})
.then(function() {
  return P.promiseAnd(Promise.resolve(false), Promise.resolve(true)).then(function(x) {
    return assert.equal(x, false, 'promiseAnd #3');
  })
})
.then(function() {
  return P.promiseAnd(Promise.resolve(false), deferredThrow('promiseAnd #4 failed')).then(function(x) {
    return assert.equal(x, false, 'promiseAnd #4');
  })
})


// test promiseOr
.then(function() {
  return P.promiseOr(false, Promise.resolve(true)).then(function(x) {
    return assert.equal(x, true, 'promiseOr #1');
  })
})
.then(function() {
  return P.promiseOr(false, Promise.resolve(false)).then(function(x) {
    return assert.equal(x, false, 'promiseOr #2');
  })
})
.then(function() {
  return P.promiseOr(false, P.promiseOr(false, Promise.resolve(false))).then(function(x) {
    return assert.equal(x, false, 'promiseOr #3');
  })
})
.then(function() {
  return P.promiseOr(Promise.resolve(true), deferredThrow('promiseOr #4 failed')).then(function(x) {
    return assert.equal(x, true, 'promiseOr #4');
  })
})


// handle errors
.catch(function(err) {
  console.log('ERROR: ' + err);
});
