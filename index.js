var Promise = require('promise');
var LazyPromise = require('lazy-promise');

// run onFulfilled after promise has been resolved
exports.when = function(promise, onFulfilled, onRejected) {
  return Promise.resolve(promise).then(onFulfilled, onRejected);
};

// eval fn then then is called
exports.deferred = function (fn) {
  return new LazyPromise(function (resolve, reject) {
    resolve(fn());
  });
};

// resolve a promise and return the negation
exports.promiseNot = function(p) {
  return Promise.resolve(p).then(function (v) {
    return !v;
  });
}

// syntactic sugar - implement with promiseAnd
exports.promiseIf = function(b,p) {
  if(b === undefined || !p) throw 'both arguments are mandatory!'

  return exports.promiseAnd(b,p);
}

// like p1() && p2()
exports.promiseAnd = function (p1, p2) {
  if(p1 === undefined || p2 === undefined) throw 'both arguments are mandatory!'
  return Promise.resolve(p1).then(function(res) {
    return res && p2;
  });
}

// like p1() || p2()
exports.promiseOr = function (p1, p2) {
  if(p1 === undefined || p2 === undefined) throw 'both arguments are mandatory!'
  return Promise.resolve(p1).then(function (res) {
    return res || p2;
  });
}
